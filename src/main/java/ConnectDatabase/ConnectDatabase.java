/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ConnectDatabase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author commis
 */
public class ConnectDatabase {
    private static ConnectDatabase instance = new ConnectDatabase();
    private Connection conn;

    private ConnectDatabase() {
    }

    public static ConnectDatabase getInstance() {

        String dbPath = "./db/FinalShop.db";
        try {
            if (instance.conn == null || instance.conn.isClosed()) {
                Class.forName("org.sqlite.JDBC");
                instance.conn = DriverManager.getConnection("jdbc:sqlite:" + dbPath);
                System.out.println("Database connection");
            }
        } catch (ClassNotFoundException ex) {
            System.out.println("Error: JDBC is not exite");
        } catch (SQLException ex) {
            System.out.println("Error: Database cannot connection");
        }
        return instance;
    }

    public static void close() {
        try {
            if (instance.conn != null && !instance.conn.isClosed()) {
                instance.conn.close();

            }
        } catch (SQLException ex) {
            Logger.getLogger(ConnectDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
        instance.conn = null;
    }
    public  Connection getConnection(){
        return instance.conn;
    }
}
