/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import ConnectDatabase.ConnectDatabase;
import Model.Customer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Van
 */
public class CustomerDao implements DaoInterface<Customer> {

    @Override
    public int add(Customer oblect) {
        Connection conn = null;
        ConnectDatabase db = ConnectDatabase.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO CUSTOMER(CUS_NAME,CUS_TEL) VALUES (?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, oblect.getName());
            stmt.setString(2, oblect.getTel());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
            return id;
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Customer> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        ConnectDatabase db = ConnectDatabase.getInstance();
        conn = db.getConnection();
        try {
            String sql = ("SELECT CUS_ID,CUS_NAME,CUS_TEL FROM CUSTOMER;");
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("CUS_ID");
                String name = result.getString("CUS_NAME");
                String tel = result.getString("CUS_TEL");
                Customer customer = new Customer(id, name, tel);
                list.add(customer);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public Customer get(int id) {
        Connection conn = null;
        ConnectDatabase db = ConnectDatabase.getInstance();
        conn = db.getConnection();
        try {
            String sql = ("SELECT CUS_ID,CUS_NAME,CUS_TEL FROM CUSTOMER WHERE CUS_ID =" + id);
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
               int cid = result.getInt("CUS_ID");
                String name = result.getString("CUS_NAME");
                String tel = result.getString("CUS_TEL");
                Customer customer = new Customer(cid, name, tel);
                return customer;
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        ConnectDatabase db = ConnectDatabase.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM CUSTOMER WHERE CUS_ID = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }

    @Override
    public int update(Customer object) {
        Connection conn = null;
        ConnectDatabase db = ConnectDatabase.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE CUSTOMER SET CUS_NAME =?,CUS_TEL=? WHERE CUS_ID = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            stmt.setInt(3, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }
}
