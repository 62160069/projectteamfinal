/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import ConnectDatabase.ConnectDatabase;
import Model.Customer;
import Model.Receipt;
import Model.ReceiptDetail;
import Model.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author BmWz
 */
public class RDao implements DaoInterface<Receipt>{

    @Override
    public int add(Receipt object) {
        Connection conn = null;
        ConnectDatabase db = ConnectDatabase.getInstance();
        conn = db.getConnection();
        int id = -1;
        //insert
        try {
            String sql = "INSERT INTO RECEIPT (CUS_ID,USER_ID,TOTAL ) VALUES (?,?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
            
             if (object.getCustomer() == null) {
                stmt.setInt(1, 0);
            } else {
                stmt.setInt(1, object.getCustomer().getId());
            }
            
            stmt.setInt(2, object.getSeller().getId());
            stmt.setDouble(3, object.getTotal());
            
            int row = stmt.executeUpdate();
            //System.out.println("row: "+row);
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
                //System.out.println("id: "+id);
//                object.setId(id);
            }
            System.out.println("-------------- Pass ---------------------------");
            for (ReceiptDetail r : object.getReceiptDetail()) {
                String sqlDetail = "INSERT INTO RECEIPT_DETAIL (RECEIPT_ID,PRODUCT_ID,AMOUNT,PRICE) VALUES (?,?,?,?)";
                PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
                stmtDetail.setInt(1, id);
                stmtDetail.setInt(2, r.getProduct().getId());
                stmtDetail.setDouble(3, r.getAmount());
                stmtDetail.setDouble(4, r.getPrice());
                int rowDetai = stmtDetail.executeUpdate();
                ResultSet resultDetai = stmt.getGeneratedKeys();
                if (resultDetai.next()) {
                    int rid = resultDetai.getInt(1);
                    r.setId(rid);
                }
            }
        } catch (SQLException ex) {
            System.out.println("Error : to create receipt" + ex.getMessage());
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Receipt> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        ConnectDatabase db = ConnectDatabase.getInstance();
        conn = db.getConnection();
        try {
            db.getConnection();
            conn.setAutoCommit(false);

            String sql = "SELECT RECEIPT_ID,DATETIME,CUS_ID,USER_ID,TOTAL FROM RECEIPT";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);

            while (result.next()) {
                int id = result.getInt("RECEIPT_ID");
                Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("DATETIME"));
                int cusId = result.getInt("CUS_ID");
                int userId = result.getInt("USER_ID");
                double total = result.getDouble("TOTAL");
                Receipt receipt = new Receipt(id, created, new User(userId, "", "", "", "", ""), new Customer(cusId, "", ""));
                list.add(receipt);
            }

            result.close();
            stmt.close();
            db.close();
        } catch (SQLException ex) {
            System.out.println("Error: Unable to select All receipt " + ex.getMessage());
        } catch (ParseException ex) {
            System.out.println("Error: Date passing All receipt " + ex.getMessage());
        }
        return list;
    }

    @Override
    public Receipt get(int id) {
        Connection conn = null;
        ConnectDatabase db = ConnectDatabase.getInstance();
        conn = db.getConnection();
        //select
        try {
            String sql = "SELECT RECEIPT_ID,\n"
                    + "       CUS_ID,\n"
                    + "       r.USER_ID as USER_ID,\n"
                    + "       u.USER_NAME as USER_NAME,\n"
                    + "       DATETIME,\n"
                    + "       TOTAL\n"
                    + "FROM RECEIPT r,USER u\n"
                    + "WHERE r.RECEIPT_ID =? and r.USER_ID = u.USER_ID;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                int rid = result.getInt("RECEIPT_ID");

                int cusId = result.getInt("CUS_ID");

                int userId = result.getInt("USER_ID");
                String userName = result.getString("USER_NAME");

                java.util.Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("DATETIME"));

                double total = result.getDouble("TOTAL");
                
                Receipt receipt = new Receipt(id, created,new User(userId, "","","","",""),new Customer(cusId,"",""));
                return receipt;
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select  receipt!!!" + ex.getMessage());
        } catch (ParseException ex) {
            System.out.println("Error : Date parsing  receipt!!!" + ex.getMessage());
        }
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        ConnectDatabase db = ConnectDatabase.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM receipt WHERE receipt_id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error : Unable to Delete id " + id);
        }

        db.close();
        return row;
    }

    @Override
    public int update(Receipt object) {
        return 0;
    }
    
}
