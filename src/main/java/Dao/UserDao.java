/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import ConnectDatabase.ConnectDatabase;
import Model.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author commis
 */
public class UserDao implements DaoInterface<User> {
    
    @Override
    public int add(User object) {
        Connection conn = null;
        ConnectDatabase db = ConnectDatabase.getInstance();
        conn = db.getConnection();
        int id = -1;
        String sql = "INSERT INTO USER(USER_USERNAME,USER_PASSWORD,USER_NAME,USER_TEL,USER_STATUS) VALUES (?,?,?,?,?)";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getUserName());
            stmt.setString(2, object.getPassword());
            stmt.setString(3, object.getName());
            stmt.setString(4, object.getTel());
            stmt.setString(5, object.getStatus());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<User> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        ConnectDatabase db = ConnectDatabase.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT USER_ID,USER_USERNAME,USER_PASSWORD,USER_NAME,USER_TEL,USER_STATUS FROM USER";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("USER_ID");
                String userName = result.getString("USER_USERNAME");
                String password = result.getString("USER_PASSWORD");
                String name = result.getString("USER_NAME");
                String tel = result.getString("USER_TEL");
                String statas = result.getString("USER_STATUS");
                User user = new User(id, userName, password, name, tel, statas);
                list.add(user);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public User get(int id) {
        Connection conn = null;
        ConnectDatabase db = ConnectDatabase.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT USER_ID,USER_USERNAME,USER_PASSWORD,USER_NAME,USER_TEL,USER_STATUS FROM USER WHERE USER_ID ="+id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int userid = result.getInt("USER_ID");
                String userName = result.getString("USER_USERNAME");
                String password = result.getString("USER_PASSWORD");
                String name = result.getString("USER_NAME");
                String tel = result.getString("USER_TEL");
                String statas = result.getString("USER_STATUS");
                User user = new User(userid, userName, password, name, tel, statas);
                return user;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        ConnectDatabase db = ConnectDatabase.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM USER WHERE USER_ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    @Override
    public int update(User object) {
        Connection conn = null;
        ConnectDatabase db = ConnectDatabase.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE USER SET USER_USERNAME = ?,USER_PASSWORD = ?,USER_NAME = ?,USER_TEL = ?,USER_STATUS = ? WHERE USER_ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,object.getUserName());
            stmt.setString(2,object.getPassword());
            stmt.setString(3, object.getName());
            stmt.setString(4, object.getTel());
            stmt.setString(5, object.getStatus());
            stmt.setInt(6, object.getId());
            row = stmt.executeUpdate();
            System.out.println("After row " + row);
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }
}
