/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import ConnectDatabase.ConnectDatabase;
import Model.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ray
 */
public class ProductDao implements DaoInterface<Product> {

    @Override
    public int add(Product object) {
        Connection conn = null;
        ConnectDatabase db = ConnectDatabase.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO PRODUCT (\n"
                    + "                        PRODUCT_ID,\n"
                    + "                        PRODUCT_NAME,\n"
                    + "                        PRODUCT_PRICE,\n"
                    + "                        PRODUCT_IMAGE\n"
                    + "                    )\n"
                    + "                    VALUES (?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setDouble(2, object.getPrice());
            stmt.setString(3, object.getImage());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
                object.setId(id);
            }
        } catch (SQLException ex) {
            System.out.println("Error: to create receipt");
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Product> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        ConnectDatabase db = ConnectDatabase.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT PRODUCT_ID,PRODUCT_NAME,PRODUCT_PRICE,PRODUCT_IMAGE FROM PRODUCT";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("PRODUCT_ID");
                String name = result.getString("PRODUCT_NAME");
                double price = result.getDouble("PRODUCT_PRICE");
                String image = result.getString("PRODUCT_IMAGE");
                Product product = new Product(id, name, price,image);
                list.add(product);
                System.out.println(product);
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public Product get(int id) {
        Connection conn = null;
        ConnectDatabase db = ConnectDatabase.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT PRODUCT_ID,\n"
                    + "       PRODUCT_NAME,\n"
                    + "       PRODUCT_PRICE\n"
                    + "       PRODUCT_IMAGE\n"
                    + "  FROM PRODUCT WHERE PRODUCT_ID=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int pid = result.getInt("PRODUCT_ID");
                String name = result.getString("PRODUCT_NAME");
                double price = result.getDouble("PRODUCT_PRICE");
                String image = result.getString("PRODUCT_IMAGE");
                Product product = new Product(pid, name, price,image);
                return product;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        ConnectDatabase db = ConnectDatabase.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM PRODUCT WHERE PRODUCT_ID = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error: Unsble to delete product id " + id + "!!");
        }
        db.close();
        return row;

    }

    @Override
    public int update(Product object) {
        Connection conn = null;
        ConnectDatabase db = ConnectDatabase.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE PRODUCT SET PRODUCT_NAME = ?, PRODUCT_PRICE = ? WHERE PRODUCT_ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setDouble(2, object.getPrice());
            stmt.setInt(3, object.getId());
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }

//    public static void main(String[] args) {
//        ProductDao dao = new ProductDao();
//        System.out.println(dao.getAll());
//        System.out.println(dao.get(3));
//        int id = dao.add(new Product(-1, "Coffe", 55.0));
//        System.out.println("id: + id");
//        System.out.println(dao.get(id));
//        Product updateProduct = dao.get(id);
//        System.out.println("update product:" + updateProduct);
//        dao.delete(id);
//        Product deleteProduct = dao.get(id);
//        System.out.println("delete product: " + deleteProduct);///
//    }
}
