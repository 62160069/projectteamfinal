/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Dao.UserDao;

/**
 *
 * @author commis
 */
public class User {
    private int id;
    private String userName;
    private String password;
    private String name;
    private String tel;
    private String status;
    private static User superAdmin = new User("super", "super","Admin", "0123456789", "Manager");
    
    public User(int id, String userName, String password, String name, String tel, String status) {
        this.id = id;
        this.userName = userName;
        this.password = password;
        this.name = name;
        this.tel = tel;
        this.status = status;
    }
    
    public User(String userName, String password, String name, String tel, String status){
        this(-1,userName,password,name,tel,status);
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", userName=" + userName + ", password=" + password + ", name=" + name + ", tel=" + tel + ", status=" + status + '}';
    }
}
