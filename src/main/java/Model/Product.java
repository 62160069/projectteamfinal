/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Dao.ProductDao;
import java.util.ArrayList;

/**
 *
 * @author ray
 */
public class Product {

    private int id;
    private String name;
    private int amount;
    private double price;
    private String image;
    

    public Product(int id, String name, int amount, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
        this.amount = amount;
    }

     public Product(int id, String name, double price, int amount) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.amount = amount;
    }
    
     public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }
    
    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Product(int id, String name, double price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
    
    public int getTotal(){
        return (int) (price*amount);
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + '}';

    }
        public static ArrayList<Product> getProductList(){
        ProductDao dao = new ProductDao();
        ArrayList<Product> list = dao.getAll();
            //System.out.println(list);
//        list.add(new Product(1,"Espresso1",40,"1.jpg"));
//        list.add(new Product(2,"Espresso2",30,"1.png"));
//        list.add(new Product(3,"Espresso3",40,"1.png"));
//        list.add(new Product(4,"Americano1",30,"2.png"));
//        list.add(new Product(5,"Americano2",40,"2.png"));
//        list.add(new Product(6,"Americano3",50,"2.png"));
//        list.add(new Product(7,"ChaYen1",40,"3.png"));
//        list.add(new Product(8,"ChaYen2",40,"3.png"));
//        list.add(new Product(9,"ChaYen3",40,"3.png"));
        return list;
    }
}
